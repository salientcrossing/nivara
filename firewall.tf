

resource "aws_networkfirewall_rule_group" "nrg" {
  count       = length(slice(data.aws_availability_zones.az.names, 0, 2))
  description = "Stateless Rate Limiting Rule"
  capacity    = 100
  name        = "Nrg${count.index}"
  type        = "STATELESS"
  rule_group {
    rules_source {
      stateless_rules_and_custom_actions {
        custom_action {
          action_definition {
            publish_metric_action {
              dimension {
                value = "2"
              }
            }
          }
          action_name = "MetricsAction"
        }
        stateless_rule {
          priority = 1
          rule_definition {
            actions = ["aws:pass", "MetricsAction"]
            match_attributes {
              source {
                address_definition = cidrsubnet(aws_vpc.vpc.cidr_block, 8, length(slice(data.aws_availability_zones.az.names, 0, 2)) + count.index)
              }
              source_port {
                from_port = 80
                to_port   = 80
              }
              destination {
                address_definition = "0.0.0.0/0"
              }
              destination_port {
                from_port = 80
                to_port   = 80
              }
              protocols = [6]
              tcp_flag {
                flags = ["SYN"]
                masks = ["SYN", "ACK"]
              }
            }
          }
        }
      }
    }
  }

  tags = {
    Tag1 = "Nrg1"
    Tag2 = "Nrg2"
  }
}

resource "aws_networkfirewall_firewall_policy" "nfp" {
  count = length(slice(data.aws_availability_zones.az.names, 0, 2))
  name  = "nfp-policy-${count.index}"

  firewall_policy {
    stateless_default_actions          = ["aws:pass"]
    stateless_fragment_default_actions = ["aws:drop"]
    stateless_rule_group_reference {
      priority     = 1
      resource_arn = aws_networkfirewall_rule_group.nrg[count.index].arn
    }
  }

  tags = {
    Tag1 = "Nfp1"
    Tag2 = "Nfp2"
  }
}

resource "aws_networkfirewall_firewall" "firewall" {
  count               = length(slice(data.aws_availability_zones.az.names, 0, 2))
  name                = "defense${count.index}"
  firewall_policy_arn = aws_networkfirewall_firewall_policy.nfp[count.index].arn
  vpc_id              = aws_vpc.vpc.id
  subnet_mapping {
    subnet_id = element(aws_subnet.sb-pub.*.id, count.index)
  }

  tags = {
    Tag1 = "Firewall1"
    Tag2 = "Firewall2"
  }
}

resource "aws_network_interface" "ni" {
  count               = length(slice(data.aws_availability_zones.az.names, 0, 2))

  subnet_id       = aws_subnet.sb-pub[count.index].id
  # private_ips     = ["10.0.${count.index + 3}.0"]
  # security_groups = [aws_security_group.sg.id]

  attachment {
    instance     = element(aws_instance.web[*].id, count.index)
    device_index = 1
  }
}