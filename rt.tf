# --------------------------------------------------------------------------------------------------------------
#                     ROUTE TABLE
# --------------------------------------------------------------------------------------------------------------

resource "aws_route_table" "i_rt" {
  count  = length(slice(data.aws_availability_zones.az.names, 0, 2))
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
    network_interface_id = aws_network_interface.ni.*.id[count.index]
  }
  tags = {
    Name = "rt_${count.index + 1}"
  }
}
resource "aws_route_table_association" "a" {
  count = length(slice(data.aws_availability_zones.az.names, 0, 2))

  subnet_id      = element(aws_subnet.sb-pub.*.id, count.index)
  route_table_id = element(aws_route_table.i_rt.*.id, count.index)
}


resource "aws_route_table_association" "b" {
  count = length(slice(data.aws_availability_zones.az.names, 0, 2))

  subnet_id      = element(aws_subnet.sb-priv.*.id, count.index)
  route_table_id = element(aws_route_table.i_rt.*.id, count.index)
}

# --------------------------------------------------------------------------------------------------------------
#                 Elastic IP configuration
# --------------------------------------------------------------------------------------------------------------
resource "aws_eip" "eip" {
  count = length(slice(data.aws_availability_zones.az.names, 0, 2))

  vpc        = true
  depends_on = [aws_internet_gateway.igw]
  tags = {
    Name = "eip_${count.index + 1}"
  }
}

# --------------------------------------------------------------------------------------------------------------
#                     NAT GATEWAY
# --------------------------------------------------------------------------------------------------------------

resource "aws_nat_gateway" "ngw" {
  count         = length(slice(data.aws_availability_zones.az.names, 0, 2))
  subnet_id     = element(aws_subnet.sb-pub.*.id, count.index)
  allocation_id = element(aws_eip.eip.*.id, count.index)
  tags = {
    Name = "Nat_Gateway_${count.index + 1}"
  }
}