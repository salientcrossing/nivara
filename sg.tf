
resource "aws_security_group" "sg" {
  name        = "hawk-grp"
  description = "Allow HTTP traffic"

  ingress {
    from_port   = local.from_http
    to_port     = local.to_http
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.vpc.id
}

resource "aws_security_group" "db-sg" {
  name        = "db-sg"
  description = "security group for RDS database"

  ingress {
    from_port   = local.db_from
    to_port     = local.db_to
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}