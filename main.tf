

resource "aws_db_instance" "db" {
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  db_name                = "robot"
  username               = var.user
  password               = var.password
  allocated_storage      = 20
  vpc_security_group_ids = flatten([aws_security_group.db-sg.id])
  skip_final_snapshot    = true

}

resource "aws_db_subnet_group" "db-subnet-grp" {
  name       = "db-subnet-grp"
  subnet_ids = flatten([aws_subnet.sb-pub.*.id, aws_subnet.sb-priv.*.id])
}

resource "aws_subnet" "sb-pub" {
  count = length(slice(data.aws_availability_zones.az.names, 0, 2))

  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 8, length(slice(data.aws_availability_zones.az.names, 0, 2)) + count.index)
  vpc_id            = aws_vpc.vpc.id
  availability_zone = slice(data.aws_availability_zones.az.names, 0, 2)[count.index]
  tags = {
    Name = "sb-${count.index + 1}"
  }
}

resource "aws_subnet" "sb-priv" {
  count = length(slice(data.aws_availability_zones.az.names, 0, 2))

  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 7, length(slice(data.aws_availability_zones.az.names, 0, 2)) + count.index + 1)
  vpc_id            = aws_vpc.vpc.id
  availability_zone = slice(data.aws_availability_zones.az.names, 0, 2)[count.index]
  tags = {
    Name = "sb-${count.index + 1}"
  }
}

resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "vpc"
  }
}
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "igw"
  }
}
