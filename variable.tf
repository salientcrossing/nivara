variable "user" {
  type = string
}

variable "password" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "region" {
  type = string
}