# resource "aws_launch_template" "instance" {
#   name = "hawk-launch-template"

#   image_id = local.ami_id

#   instance_type = "t2.micro"

# }

resource "aws_lb" "lb" {
  name               = "alb-lb-tf"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.sg.id]
  subnets            = flatten([aws_subnet.sb-pub.*.id])


  enable_deletion_protection = false

  #   access_logs {
  #     bucket  = aws_s3_bucket.lb_logs.bucket
  #     prefix  = "test-lb"
  #     enabled = true
  #   }

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_target_group" "lb_target" {
  name     = "target-grp"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc.id

  health_check {
    path                = "/"
    interval            = 30
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 2
  }
}

resource "aws_lb_listener" "lb_listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_target.arn
  }
}

resource "aws_lb_target_group_attachment" "altga" {
  count            = length(slice(data.aws_availability_zones.az.names, 0, 2))
  target_group_arn = aws_lb_target_group.lb_target.arn
  target_id        = aws_instance.web[count.index].id
  port             = 80
}

resource "aws_instance" "web" {
  count                       = length(slice(data.aws_availability_zones.az.names, 0, 2))
  ami                         = local.ami_id
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  subnet_id                   = element(aws_subnet.sb-pub.*.id, count.index)

  tags = {
    Name = "web-${count.index}"
  }
}