FROM ubuntu:18.04

COPY index.html /
COPY error.html /
COPY setup.sh /

RUN bash setup.sh

EXPOSE 80

CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]